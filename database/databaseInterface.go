package database

type DatabaseAPI interface {
	ListAll() []map[string]interface{}
	Get(condition map[string]string) []map[string]interface{}
	Create(value interface{}) (bool, error)
	Delete(condition string) (bool, error)
	Update(value interface{}) (bool, error)
}

type DbConfig struct {
	Host   string `json:"host"`
	User   string `json:"user"`
	DbName string `json:"dbname"`
	Port   string `json:"port"`
}
