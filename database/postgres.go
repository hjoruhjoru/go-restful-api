package database

import (
	"encoding/json"
	"fmt"
	"io/ioutil"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type Postgres struct {
	DatabaseAPI
	dbSession *gorm.DB
}

func NewPostgresDb() Postgres {
	dbConfigFile, _ := ioutil.ReadFile("config/db.json")
	var dbConfig DbConfig
	json.Unmarshal(dbConfigFile, &dbConfig)
	dsn := fmt.Sprintf("host=%s user=%s dbname=%s port=%s", dbConfig.Host, dbConfig.User, dbConfig.DbName, dbConfig.Port)
	dbSession, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		fmt.Println("create db session error")
	}
	return Postgres{dbSession: dbSession}
}

func (protgresDb Postgres) ListAll() []map[string]interface{} {
	var results []map[string]interface{}
	protgresDb.dbSession.Table("users").Find(&results)
	return results
}

func (protgresDb Postgres) Get(condition map[string]string) []map[string]interface{} {
	var results []map[string]interface{}
	dbTable := protgresDb.dbSession.Table("users")
	for column, val := range condition {
		dbTable.Where(column+" = ?", val)
	}
	dbTable.Find(&results)
	return results
}
