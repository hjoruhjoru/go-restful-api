package main

import (
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"go-restful-api/helper"
	"go-restful-api/middleware"
	"go-restful-api/model"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

func main() {
	r := mux.NewRouter()
	getApi := r.Methods(http.MethodGet).Subrouter()
	getApi.HandleFunc("/users", getAllUsers)
	getApi.HandleFunc("/users/{fullname}", getUserByFullName)
	getApi.HandleFunc("/user/me", getMe)
	getApi.Use(middleware.Authentication)
	getApi.Use(middleware.ContentTypeApplicationJson)

	deleteApi := r.Methods(http.MethodDelete).Subrouter()
	deleteApi.HandleFunc("/user", deleteMe)
	deleteApi.Use(middleware.Authentication)
	deleteApi.Use(middleware.ContentTypeApplicationJson)

	signApi := r.Methods(http.MethodPost).Subrouter()
	signApi.HandleFunc("/user", createUser)
	signApi.HandleFunc("/token", signIn)
	signApi.Use(middleware.ContentTypeApplicationJson)

	updateApi := r.Methods(http.MethodPut).Subrouter()
	updateApi.HandleFunc("/user", updateMe)
	updateApi.Use(middleware.Authentication)
	updateApi.Use(middleware.ContentTypeApplicationJson)

	http.ListenAndServe(":8080", r)
}

func getAllUsers(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	users := model.ListAllUsers()
	usersJson, _ := json.Marshal(users)
	response := fmt.Sprintf("{\"message\": \"OK\", \"data\": %s}", string(usersJson))
	fmt.Fprintf(w, response)
}

func getUserByFullName(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	user := model.GetUserByFullName(vars["fullname"])
	if len(user) == 0 {
		fmt.Fprintf(w, "{\"message\": \"OK\", \"data\": []}")
		return
	}
	w.WriteHeader(http.StatusOK)
	usersJson, _ := json.Marshal(user)
	response := fmt.Sprintf("{\"message\": \"OK\", \"data\": %s}", string(usersJson))
	fmt.Fprintf(w, response)
}

func getMe(w http.ResponseWriter, r *http.Request) {
	acct := r.Header.Get("acct")
	user := model.GetUserByAcct(acct)
	if len(user) == 0 {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("User not found"))
		return
	}
	w.WriteHeader(http.StatusOK)
	usersJson, _ := json.Marshal(user)
	fmt.Println(user)
	response := fmt.Sprintf("{\"message\": \"OK\", \"data\": %s}", string(usersJson))
	fmt.Fprintf(w, response)
}

func createUser(w http.ResponseWriter, r *http.Request) {
	var user model.User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	now := time.Now()
	user.Pwd = getMd5Hash(user.Pwd)
	user.CreateAt = now.Format(time.RFC3339)
	user.UpdateAt = now.Format(time.RFC3339)
	isSuccess := model.CreateUser(user)
	if !isSuccess {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Create user fail"))
		return
	}
	w.WriteHeader(http.StatusOK)
}

func getMd5Hash(text string) string {
	hash := md5.Sum([]byte(text))
	return hex.EncodeToString(hash[:])
}

func signIn(w http.ResponseWriter, r *http.Request) {
	var user model.User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	user.Pwd = getMd5Hash(user.Pwd)
	isUserValid := model.IsUserValid(user.Acct, user.Pwd)
	if !isUserValid {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "{\"message\": \"Invalid user\", \"data\": {}")
		return
	}
	fmt.Fprintf(w, fmt.Sprintf("{\"message\": \"OK\", \"data\": {\"token\":\"%s\"}}", helper.GenerateJwtToken(user.Acct, user.Fullname)))
}

func deleteMe(w http.ResponseWriter, r *http.Request) {
	acct := r.Header.Get("acct")
	model.DeleteUser(acct)
}

func updateMe(w http.ResponseWriter, r *http.Request) {
	var user model.User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	acct := r.Header.Get("acct")
	now := time.Now()
	user.Pwd = getMd5Hash(user.Pwd)
	user.UpdateAt = now.Format(time.RFC3339)
	isSuccess := model.UpdateUser(acct, user.Pwd, user.Fullname, user.UpdateAt)
	if !isSuccess {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Update user fail"))
		return
	}
	w.WriteHeader(http.StatusOK)
}
