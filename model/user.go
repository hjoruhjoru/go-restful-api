package model

import (
	"encoding/json"
	"fmt"
	"go-restful-api/database"
	"io/ioutil"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type DbConfig struct {
	Host   string `json:"host"`
	User   string `json:"user"`
	DbName string `json:"dbname"`
	Port   string `json:"port"`
}

type User struct {
	Acct     string `json:"acct"`
	Pwd      string `json:"pwd"`
	Fullname string `json:"fullname"`
	CreateAt string `json:"create_at"`
	UpdateAt string `json:"update_at"`
}

func initDb() (*gorm.DB, error) {
	dbConfigFile, _ := ioutil.ReadFile("config/db.json")
	var dbConfig DbConfig
	json.Unmarshal(dbConfigFile, &dbConfig)

	dsn := fmt.Sprintf("host=%s user=%s dbname=%s port=%s", dbConfig.Host, dbConfig.User, dbConfig.DbName, dbConfig.Port)
	return gorm.Open(postgres.Open(dsn), &gorm.Config{})
}

func ListAllUsers() []map[string]interface{} {
	db := database.NewPostgresDb()
	return db.ListAll()
}

func GetUserByFullName(fullName string) []map[string]interface{} {
	db := database.NewPostgresDb()
	return db.Get(map[string]string{"fullname": fullName})
}

func GetUserByAcct(acct string) []map[string]interface{} {
	db := database.NewPostgresDb()
	return db.Get(map[string]string{"acct": acct})
}

func CreateUser(user User) bool {
	db, err := initDb()
	if err != nil {
		return false
	}
	result := db.Select("acct", "pwd", "fullname", "create_at", "update_at").Create(&user)
	if result.Error != nil {
		return false
	}
	return true
}

func IsUserValid(acct string, pwd string) bool {
	db, err := initDb()
	if err != nil {
		return false
	}
	var user map[string]interface{}
	db.Table("users").Select("acct").Where("acct = ? and pwd = ?", acct, pwd).Find(&user)
	if len(user) == 0 {
		return false
	}
	return true
}

func DeleteUser(acct string) {
	db, err := initDb()
	if err != nil {
		return
	}
	db.Where("acct = ?", acct).Delete(&User{})
}

func UpdateUser(acct string, pwd string, fullname string, updateAt string) bool {
	db, err := initDb()
	if err != nil {
		return false
	}
	result := db.Table("users").Where("acct = ?", acct).Updates(User{Pwd: pwd, Fullname: fullname, UpdateAt: updateAt})
	if result.Error != nil {
		return false
	}
	return true
}
