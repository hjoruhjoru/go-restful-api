package middleware

import (
	"net/http"
	"go-restful-api/helper"
)

func Authentication(next http.Handler) http.Handler {
    return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		token := r.Header.Get("Authentication")
		tokenUser, isValidate := helper.ValidateToken(token)
		if !isValidate {
			w.WriteHeader(http.StatusUnauthorized)
			w.Write([]byte("Invalid token"))
			return
		}
		r.Header.Set("acct", tokenUser.Acct)
		r.Header.Set("fullname", tokenUser.FullName)
        next.ServeHTTP(w, r)
    })
}