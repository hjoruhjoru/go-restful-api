package helper

import (
	"github.com/golang-jwt/jwt"
	"io/ioutil"
	"encoding/json"
	"time"
)

type JwtConfig struct {
	TokeExpiration int `json:"toke_expiration"`
	Issuer string `json:"issuer"`
}

type Claims struct {
	Acct string `json:"acct"`
	FullName string `json:"full_name"`
	jwt.StandardClaims
}

func getPublicKey() []byte {
	publicKey, err := ioutil.ReadFile("config/jwtRS256.key.pub")
	if err != nil {
		return []byte{}
	}
	return publicKey
}

func getPrivateKey() []byte {
	privateKey, err := ioutil.ReadFile("config/jwtRS256.key")
	if err != nil {
		return []byte{}
	}
	return privateKey
}

func GenerateJwtToken(acct string, fullName string) string {
	jwtConfigFile , _ := ioutil.ReadFile("config/jwt.json")
	var jwtConfig JwtConfig
	json.Unmarshal(jwtConfigFile, &jwtConfig)

	now := time.Now()
	claims := Claims{
		Acct: acct,
		FullName: fullName,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: now.Add(time.Duration(jwtConfig.TokeExpiration) * time.Second).Unix(),
			IssuedAt: now.Unix(),
			Issuer:	jwtConfig.Issuer,
		},
	}

	token := jwt.New(jwt.GetSigningMethod("RS256"))
	token.Claims = &claims
	privateKey := getPrivateKey()
	signKey, _ := jwt.ParseRSAPrivateKeyFromPEM(privateKey)
	tokenString, err := token.SignedString(signKey)
	if err != nil {
		return ""
	}
	return tokenString
}

func ValidateToken(tokenString string) (Claims, bool) {
	var claims Claims
    _, err := jwt.ParseWithClaims(tokenString, &claims, func(token *jwt.Token) (interface{}, error) {
		publicKey := getPublicKey()
		verifyKey, _ := jwt.ParseRSAPublicKeyFromPEM(publicKey)
		return verifyKey, nil
	})
	if err != nil {
		return  claims, false
	}
	return claims, true 
}
