[ -e config/jwtRS256.key ] && rm config/jwtRS256.key
[ -e config/jwtRS256.key.pub ] && rm config/jwtRS256.key.pub
ssh-keygen -t rsa -b 4096 -m PEM -f config/jwtRS256.key -q -N ""
openssl rsa -in config/jwtRS256.key -pubout -outform PEM -out config/jwtRS256.key.pub

mkdir -p volume
docker stop postgres-db || true && docker rm postgres-db || true
docker rmi $(docker images --format '{{.Repository}}:{{.Tag}}' | grep 'postgres')
docker pull postgres
docker run --name postgres-db -e POSTGRES_USER=test -e POSTGRES_DB=test -e POSTGRES_HOST_AUTH_METHOD=trust -p 5432:5432 -v volume:/var/lib/postgresql/data -d postgres
sleep 5

cat sql/user.sql| docker exec -i postgres-db psql -U test -d test
sleep 3

go run api/main.go