# go-restful-api 
The project is how to design go restful api

## Getting started

unzip folder and enter the folder
```
unzip ui-assignment.zip
cd ui-assignment
git checkout feat/impl-api
```

## Environment Setup

Generate JWT key pair, setup postgre, create table and start server

```
sh setup.sh
```

## Api description

1. list all users.
GET /users
```
curl http://127.0.0.1:8080/users --header "Authentication: {token}"
```

2. Search an user by fullname
GET /users/{fullname}
```
curl http://127.0.0.1:8080/users/{fullname} --header "Authentication: {token}"
```

3. Get the user’s detailed information
GET /user
```
curl http://127.0.0.1:8080/user --header "Authentication: {token}"
```

4. Create the user (user sign up)
POST /user
```
curl -XPOST http://127.0.0.1:8080/user -H 'Content-Type: application/json' -d '{"acct": "{acct}", "pwd":"{pwd}", "fullname":"{fullname}"}'
```

5. Generate the token to the user (user sign in).
POST /token 
```
curl -XPOST http://127.0.0.1:8080/token -H 'Content-Type: application/json' -d '{"acct": "{acct}", "pwd":"{pwd}", "fullname":"{fullname}"}'
```

6. Delete the user
DELETE /user
```
curl -XDELETE http://127.0.0.1:8080/user --header "Authentication: {token}"
``` 

7. Update the user
PUT /user
```
curl -XPUT http://127.0.0.1:8080/user -H 'Content-Type: application/json' -d '{"pwd":"{pwd}", "fullname":"{fullname}"}' --header "Authentication: {token}"
```